from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import random
import os.path

class BasePage:
    LOCATORS = {}


    def __init__(self, driver):
        self.driver = driver
        #self.base_url = URLs.BASE_URL

    def basic_auth(self):
        obj = self.driver.switch_to.alert
        obj.accept()
        #obj.send_keys(keysToSend="admin\ue004securetrading")

    def reload_current_page(self):
        self.driver.refresh()

    def back(self):
        self.driver.back()
        #self.execute_script("window.history.go(-1)")

    def get_current_url(self):
        return self.driver.current_url

    def wait_until_clickable(self, locator, time=17):
        return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_element(self, locator, time=7):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=5):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def invisibility_of_element(self, locator_name, time=2):
        return WebDriverWait(self.driver, time).until(EC.invisibility_of_element(self.LOCATORS[locator_name]),
                                                      message=f"Can't find element by locator {locator_name}")

    def is_element_displayed(self, locator_name_for_element):
        element = self.find_element(
            self.LOCATORS[locator_name_for_element])
        return element.is_displayed()

    def go_to_site(self, url):
        return self.driver.get(url)

    def open_local_html(self, file_name_to_open):
        data_folder = os.path.join(os.path.abspath(os.getcwd()),"features", "pages", "PaymentPages")
        page = os.path.join(data_folder, file_name_to_open)
        return self.driver.get("file:///"+str(page))

    def switch_to_window(self, number):
        return self.driver.switch_to.window(self.driver.window_handles[number])

    def switch_to_frame(self, frame):
        return self.driver.switch_to_frame(frame)

    def switch_to_default_content(self):
        return self.driver.switch_to_default_content()

    def change_value_in_text_field(self, locator_name_for_text_field, value):
        text_field = self.find_element(
            self.LOCATORS[locator_name_for_text_field])
        self.wait_until_clickable(self.LOCATORS[locator_name_for_text_field])
        text_field.clear()
        text_field.send_keys(value)

    def get_text_value_from_text_field(self, locator_name_for_text_field):
        text_field = self.find_element(
            self.LOCATORS[locator_name_for_text_field])
        return text_field.get_attribute('value')

    def execute_script(self, script):
        return self.driver.execute_script(script)

    def change_attribute_value(self, locator_name_for_element, attribute_name, new_value):
         element = self.find_element(self.LOCATORS[locator_name_for_element])
         script_arguments = ["arguments[0].setAttribute(","'",attribute_name,"',","'",new_value,"'",")"]
         script = ''.join(script_arguments)
         self.driver.execute_script(script, element);

    def delete_attribute(self, element, attribute_name):
         #element = self.find_element(self.LOCATORS[locator_name_for_element])
         script_arguments = ["arguments[0].removeAttribute(","'",attribute_name,"'",")"]
         script = ''.join(script_arguments)
         self.driver.execute_script(script, element);


    def change_innertext_for_element(self, locator_name_for_element, new_value):
         element = self.find_element(self.LOCATORS[locator_name_for_element])
         script_arguments = ["arguments[0].innerHTML = ","'",new_value,"'",";"]
         script = ''.join(script_arguments)
         self.driver.execute_script(script, element);

    def select_option_for_drop_down_list(self, field_name, desired_option):
        drop_down_field = Select(
            self.find_element(self.LOCATORS[field_name]))
        if not (drop_down_field.all_selected_options[0].text.lower().startswith(desired_option)):
            drop_down_field.select_by_visible_text(desired_option)

    def select_option_by_index_for_drop_down_list(self, field_name, desired_option):
        drop_down_field = Select(
            self.find_element(self.LOCATORS[field_name]))
        drop_down_field.select_by_value(desired_option)


    def disable_checkbox(self, locator_name_for_checkbox):
        selected_checkbox = self.find_element(
            self.LOCATORS[locator_name_for_checkbox])
        if selected_checkbox.is_selected():
            selected_checkbox.click()

    def enable_checkbox(self, locator_name_for_checkbox):
        selected_checkbox = self.find_element(
            self.LOCATORS[locator_name_for_checkbox])
        if not selected_checkbox.is_selected():
            selected_checkbox.click()

    def get_text_from_element(self, locator_to_element):
        element = self.find_element(self.LOCATORS[locator_to_element])
        return element.text

    def get_text_from_elements(self, locator_to_elements):
        text=[]
        elements = self.find_elements(self.LOCATORS[locator_to_elements])
        for i in range(len(elements)):
            text.append(elements[i].text)
        return  text

    def get_attribute_value_from_element(self, locator_name_for_element, attribute_name):
        element = self.find_element(self.LOCATORS[locator_name_for_element])
        return element.get_attribute(attribute_name)

    def get_css_from_element(self, locator_name_for_element, property_name):
        element = self.find_element(self.LOCATORS[locator_name_for_element])
        return element.value_of_css_property(property_name)

    def click_on_element(self, locator_name_for_element):
        self.wait_until_clickable(self.LOCATORS[locator_name_for_element])
        element = self.find_element(
            self.LOCATORS[locator_name_for_element])
        element.click()

    def click_on_element_number_from_collection(self, locator_name_for_collection, number):
        elements = self.find_elements(
            self.LOCATORS[locator_name_for_collection])
        elements[number].click()

    def click_on_any_element_from_collection(self, locator_name_for_collection):
        elements = self.find_elements(
            self.LOCATORS[locator_name_for_collection])
        random_item = random.choice(elements).click()

    def click_on_any_element_from_reverse_collection(self, locator_name_for_collection):
        elements = self.find_elements(
            self.LOCATORS[locator_name_for_collection])[:]
        random_item = random.choice(elements).click()


    def one_or_more_options(self, locator_name_for_element):
        return len(self.find_elements(self.LOCATORS[locator_name_for_element]))

    def send_ENTER_key_to_field(self, locator_name_for_element):
        text_field = self.find_element(
            self.LOCATORS[locator_name_for_element])
        text_field.send_keys(Keys.ENTER)

    def get_list_of_iframes(self):
        return self.driver.find_elements_by_tag_name("iframe")

    def drop_files(element, files, offsetX=0, offsetY=0):
        JS_DROP_FILES = "var c=arguments,b=c[0],k=c[1];c=c[2];for(var d=b.ownerDocument||document,l=0;;){var e=b.getBoundingClientRect(),g=e.left+(k||e.width/2),h=e.top+(c||e.height/2),f=d.elementFromPoint(g,h);if(f&&b.contains(f))break;if(1<++l)throw b=Error('Element not interactable'),b.code=15,b;b.scrollIntoView({behavior:'instant',block:'center',inline:'center'})}var a=d.createElement('INPUT');a.setAttribute('type','file');a.setAttribute('multiple','');a.setAttribute('style','position:fixed;z-index:2147483647;left:0;top:0;');a.onchange=function(b){a.parentElement.removeChild(a);b.stopPropagation();var c={constructor:DataTransfer,effectAllowed:'all',dropEffect:'none',types:['Files'],files:a.files,setData:function(){},getData:function(){},clearData:function(){},setDragImage:function(){}};window.DataTransferItemList&&(c.items=Object.setPrototypeOf(Array.prototype.map.call(a.files,function(a){return{constructor:DataTransferItem,kind:'file',type:a.type,getAsFile:function(){return a},getAsString:function(b){var c=new FileReader;c.onload=function(a){b(a.target.result)};c.readAsText(a)}}}),{constructor:DataTransferItemList,add:function(){},clear:function(){},remove:function(){}}));['dragenter','dragover','drop'].forEach(function(a){var b=d.createEvent('DragEvent');b.initMouseEvent(a,!0,!0,d.defaultView,0,0,0,g,h,!1,!1,!1,!1,0,null);Object.setPrototypeOf(b,null);b.dataTransfer=c;Object.setPrototypeOf(b,DragEvent.prototype);f.dispatchEvent(b)})};d.documentElement.appendChild(a);a.getBoundingClientRect();return a;"

        driver = element.parent #self.driver #
        isLocal = not driver._is_remote or '127.0.0.1' in driver.command_executor._url
        paths = []

        # ensure files are present, and upload to the remote server if session is remote
        for file in (files if isinstance(files, list) else [files]) :
            if not os.path.isfile(file) :
                raise FileNotFoundError(file)
            paths.append(file if isLocal else element._upload(file))

        value = '\n'.join(paths)
        elm_input = driver.execute_script(JS_DROP_FILES, element, offsetX, offsetY)
        elm_input._execute('sendKeysToElement', {'value': [value], 'text': value})

    WebElement.drop_files = drop_files


