from pages.BaseApp import BasePage
from selenium.webdriver.common.by import By
import time


class WeatherForecastHomePage(BasePage):
    LOCATORS = {
        'location_field': (By.ID, 'location'),
        'search_button': (By.ID, 'searchbtn')

    }

    def enter_location(self, location):
        time.sleep(5)
        self.change_value_in_text_field('location_field', location)

    def submit_search(self):
        time.sleep(5)
        if self.find_element(self.LOCATORS['search_button']).is_displayed():
            self.click_on_element('search_button')
