from pages.BaseApp import BasePage
from selenium.webdriver.common.by import By
import time


class LocationWeatherForecastPage(BasePage):
    LOCATORS = {
        'forecast_header': (By.XPATH, "//h1"),
        'weather_station': (By.XPATH, "//h2[@class='b-metar__title columns']"),
        'weather_today': (By.XPATH, "//div[contains(text(),('Today'))]")
    }

    def get_location_header(self):
        time.sleep(15)
        return self.get_text_from_element('forecast_header')

    def get_weather_station_header(self):
        time.sleep(5)
        return self.get_text_from_element('weather_station')

    def get_today_weather(self):
        time.sleep(5)
        return self.get_text_from_element('weather_today')
