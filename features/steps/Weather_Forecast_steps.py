from behave import *

from pages.WeatherForecast.WeatherForecastHomePage import WeatherForecastHomePage
from pages.WeatherForecast.LocationWeatherForecastPage import LocationWeatherForecastPage


@given("I am on the weather forecast page")
def step_impl(context):
    context.weather_forecast_page = WeatherForecastHomePage(context.browser)
    context.weather_forecast_page.go_to_site(context.base_url)


@when("I enter a {city}")
def step_impl(context, city):
    context.weather_forecast_page.enter_location(city)


@step('I click on "Search" button')
def step_impl(context):
    context.weather_forecast_page.submit_search()


@then('Header "{city} Weather Forecast" is displayed')
def step_impl(context, city):
    context.location_weather_forecast_page = LocationWeatherForecastPage(context.browser)
    title = context.location_weather_forecast_page.get_location_header()
    assert (city in title)


@then('Header "Live weather stations near {city}" is displayed')
def step_impl(context, city):
    context.location_weather_forecast_page = LocationWeatherForecastPage(context.browser)
    title = context.location_weather_forecast_page.get_weather_station_header()
    assert (city in title)


@then('I am able to see "{city} Live Weather Today"')
def step_impl(context, city):
    context.location_weather_forecast_page = LocationWeatherForecastPage(context.browser)
    title = context.location_weather_forecast_page.get_today_weather()
    assert (city in title)
