from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import datetime
import os
import json
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import IEDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from browserstack.local import Local
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# Hooks
CONFIG_FILE = os.environ['CONFIG_FILE'] if 'CONFIG_FILE' in os.environ else 'config/run web.json'
TASK_ID = int(os.environ['TASK_ID']) if 'TASK_ID' in os.environ else 0
MOBILE_CONFIG_FILE = os.environ[
    'MOBILE_CONFIG_FILE'] if 'MOBILE_CONFIG_FILE' in os.environ else 'config/run mobile.json'

with open(CONFIG_FILE) as data_file:
    CONFIG = json.load(data_file)

ENVIRONMENT_FILE = os.environ['ENVIRONMENT_FILE'] if 'ENVIRONMENT_FILE' in os.environ else 'config/env.json'
with open(ENVIRONMENT_FILE) as data_file:
    ENVIRONMENT = json.load(data_file)

BROWSER = os.environ[
    'BROWSER'] if 'BROWSER' in os.environ else 'chrome_headless'  # 'chrome_headless'/'chrome'/'firefox'/'EDGE/browserstack'
TESTING_NOTES = os.environ['TESTING_NOTES'] if 'TESTING_NOTES' in os.environ else "BUILD 0.0.0.1 - Autotests"

with open('test_data.json') as json_file:
    TEST_DATA = json.load(json_file)

bs_local = None

BROWSERSTACK_USERNAME = os.environ['BROWSERSTACK_USERNAME'] if 'BROWSERSTACK_USERNAME' in os.environ else CONFIG['user']
BROWSERSTACK_ACCESS_KEY = os.environ['BROWSERSTACK_ACCESS_KEY'] if 'BROWSERSTACK_ACCESS_KEY' in os.environ else CONFIG[
    'key']


def start_local():
    """Code to start browserstack local before start of test."""
    global bs_local
    bs_local = Local()
    bs_local_args = {"key": BROWSERSTACK_ACCESS_KEY, "forcelocal": "true"}
    bs_local.start(**bs_local_args)


def stop_local():
    """Code to stop browserstack local after end of test."""
    global bs_local
    if bs_local is not None:
        bs_local.stop()


def before_scenario(context, scenario):
    context.testing_name = TESTING_NOTES

    context.environment = ENVIRONMENT['env']
    context.base_url = ENVIRONMENT['base_url']
    context.gitlab_chrome_url = ENVIRONMENT['gitlab_chrome_url']

    context.test_data = TEST_DATA
    context.test_data['config']['env'] = context.environment
    context.test_data['config']['base_url'] = context.base_url
    context.test_data['config']['gitlab_chrome_url'] = context.gitlab_chrome_url
    context.site = ""
    #gitlab_chrome_url = "http://selenium__standalone-chrome:4444/wd/hub"

    if 'web' in context.tags:
        if BROWSER == 'chrome_headless':
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=1920x1080")
            chrome_options.add_argument("--ignore-certificate-errors")
            chrome_options.add_argument("--ignore-urlfetcher-cert-requests")
            chrome_options.add_argument('--allow-running-insecure-content')
            context.browser = webdriver.Chrome(
                ChromeDriverManager().install(), chrome_options=chrome_options)
        if BROWSER == 'chrome':
            chrome_options = Options()
            chrome_options.add_argument("--ignore-certificate-errors")
            chrome_options.add_argument("--ignore-urlfetcher-cert-requests")
            chrome_options.add_argument('--allow-running-insecure-content')
            context.browser = webdriver.Chrome(
                ChromeDriverManager().install(), chrome_options=chrome_options)
            context.browser.set_page_load_timeout(40)
            context.browser.maximize_window()
        elif BROWSER == 'gitlab_chrome':
            context.browser = webdriver.Remote(
                desired_capabilities=DesiredCapabilities.CHROME,
                command_executor=context.gitlab_chrome_url)
            context.browser.set_page_load_timeout(40)
            context.browser.maximize_window()
        elif BROWSER == 'firefox':
            profile = webdriver.FirefoxProfile()
            profile.accept_untrusted_certs = True
            context.browser = webdriver.Firefox(
                executable_path=GeckoDriverManager().install(), firefox_profile=profile)
            context.browser.maximize_window()
        elif BROWSER == 'IE':
            context.browser = webdriver.Ie(IEDriverManager().install())
            context.browser.maximize_window()
        elif BROWSER == 'EDGE':
            context.browser = webdriver.Edge(EdgeChromiumDriverManager().install())
            context.browser.maximize_window()
        elif BROWSER == 'browserstack':
            desired_capabilities = CONFIG['environments'][TASK_ID]
            for key in CONFIG["capabilities"]:
                if key not in desired_capabilities:
                    desired_capabilities[key] = CONFIG["capabilities"][key]
            if "browserstack.local" in desired_capabilities and desired_capabilities["browserstack.local"]:
                start_local()

            desired_capabilities['name'] = context.scenario.name
            desired_capabilities['project'] = str(sorted(context.tags))
            desired_capabilities['build'] = TESTING_NOTES
            context.browser = webdriver.Remote(
                desired_capabilities=desired_capabilities,
                command_executor="http://%s:%s@hub.browserstack.com/wd/hub" % (
                BROWSERSTACK_USERNAME, BROWSERSTACK_ACCESS_KEY)
            )
            context.browser.maximize_window()


def before_step(context, step):
    context.step = step


def after_step(context, step):
    if 'web' in context.tags:
        if BROWSER == 'chrome':
            file = open('console_errors.txt', 'a')
            for entry in context.browser.get_log('browser'):
                file.write(context.browser.title + ' ==> ' + context.browser.current_url +
                           ': \n' + str(entry) + '\n' + '--------------------' + '\n')
            file.close()


def after_scenario(context, step):
    if 'web' in context.tags:
        if step.status == "failed":
            context.browser.save_screenshot("./screenshots/" + f"{context.scenario.name}.png")
    if 'web' in context.tags:
        context.browser.quit()
        stop_local()
    context.config.setup_logging()