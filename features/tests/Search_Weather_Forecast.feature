@web @test
Feature: Weather Forecast

  Scenario Outline: Search for weather forecast
    Given I am on the weather forecast page
    When I enter a <city>
    And I click on "Search" button
    Then Header "<city> Weather Forecast" is displayed
    Then Header "Live weather stations near <city>" is displayed
    Then I am able to see "<city> Live Weather Today"

    Examples: Cities
      | city   |
      | Minsk  |
      | Moscow |
