### Setup

This project uses [Python 3 with pipenv]

> Make sure your browsers  use correct certificates

1. Put your env name in **config\env.json** :

```js
{
  "env": "https://10.4.65.3",
  "base_url_myst": "https://myst.securetrading.net/login" ,
  "datacenterurl": "https://webservices.securetrading.net",
  "payments_url": "https://payments.securetrading.net"
}
```

this **env** will be used to take root credentials from here: **features\data\Data.py** 


```js
class Data:
    LOGIN_DETAILS = {       
        'https://10.4.65.3': {
            'root': {
                    'username': 'mystroot@securetrading.com',
                    'password': 'ata4pdq',
                    'auth_code': '7LZ2UKHREO6MAWXM5FJRX2UAVORAH34S',
                    'site_rule': 'root'
                }
        },
        'https://your_desired_dev_box': { 
            'root': {
                    'username': 'mystroot@securetrading.com',
                    'password': 'ata4pdq',
                    'auth_code': '7LZ2UKHREO6MAWXM5FJRX2UAVORAH34S',
                    'site_rule': 'root'
                }
        }
    }
```
if you want to use another **env** just put its name in **env.json** and put root credentials for it by changing **'https://your_desired_dev_box'** and **root** details

2. to  change browser please use features\environment.py - you need to change the value in this string:

 ```python
 BROWSER = os.environ['BROWSER'] if 'BROWSER' in os.environ else 'chrome' # change ‘chrome’ to any of'chrome'/'firefox'/'EDGE
 if you need to run your tests in headless mode please use - 'chrome_headless'
 ```



> NOTE: you do not need to download a webdriver locally it will be downloaded automatically after installation of all libraries from pip file (requirements.txt)

 

3. All test data created during testing are stored in **test_data.json** (most of this data is used in automation tests).

> **Please delete all values from test_data.json before run tests.**

4. If you are using Chrome and if there are some errors in the browser’s console, you can find them in **console_errors.txt**

5. In case of a failed test for the web, the screenshot will be created and stored in the “**screenshots**“ folder

6. If you are doing webservices regression - the last request/response can be found in **request_json.txt/response_json.txt/response_xml.txt**  you can look into it in case of failures.

7. by custom tagging, you can debug/re-run failed tests (e.g. just put any tag name before tests you want to rerun (e.g. @my_rerun) and type in console ```behave --tags @my_rerun ``` )
____________________
### Test Execution

Install Python - https://www.python.org/downloads/

open it via IDE (e.g. PyCharm/Visual Studio Code etc.) or open console(terminal) in the unzipped framework folder

```pipenv --python 3.8``` - to create env

```pipenv install``` - to install all dependencies from pipfile

```pipenv shell``` - to activate env

to run tests please type in console: e.g. ```behave --tags @e2e```
_____________________

#### run all tests:  
```behave```

#### filter tests by feature file: 
```behave features/name_of_feature_file.feature```

#### filter tests by tags:  
```behave --tags @e2e```

#### if you want to create users/sites  only -  please use @site_users_creation tag:
```behave --tags @site_users_creation```

#### to run the regression for webservices only, before e2e suite should be run

```behave --tags @webservices_regression```

#### to run the regression for webservices after 'e2e' tests

```behave --tags="@e2e, @webservices_regression"```


#### to run the regression for payment pages only, before e2e/site_users_creation suite should be run

```behave --tags @payment_pages``` 

#### to run the regression for payment pages after tests with @site_users_creation tag

```behave --tags="@site_users_creation, @payment_pages"```

#### to run the regression for payment pages after 'e2e' tests

```behave --tags="@e2e, @payment_pages"``` 


```bash
### to run specific tests for payment page :
behave --tags @ST_MPI_PPg_v1

behave --tags @ST_MPI_PPg_v2

behave --tags @cardinal_MPI_PPg_v1

behave --tags @cardinal_MPI_PPg_v2

behave --tags @action_buttons

behave --tags @clicking_back

behave --tags @url_redirect

behave --tags @card_type_2

behave --tags @card_type_1

behave --tags @dcc

behave --tags @request_types

behave --tags @moto
```

## Please note: 
1. Mobile testing willbe done via BrowserStack
2. To identify tests in BrowserStack please add value to 'TESTING_NOTES' constant e.g. "BUILD 0.0.0.1 - Autotests"
3. Set any android/ios device and put it's name/os_version in first element for "environment" node.
```js
  "environments": [
    {
      "device": "Samsung Galaxy S10e",
      "os_version": "9.0"
    },
```


#### to run regression for payment pages on Android devices (before run please set device in '/config/run mobile.json' - first item in 'environments'): 
```behave --tags @android``` 

#### to run regression for payment pages on IOS devices (before run please set device in '/config/run mobile.json' - first item in 'environments'):
```behave --tags @ios``` 

-----------------------------------------------

#### print JUnit report

```behave --junit```

#### print Json report

```behave --format=json -o test_results_data.json```

#### print HTML Report
```behave -f html --tags @debug -o test_results_data.html```


_____________________

#### Make sure your `hosts` file contains all information:

e.g.
```bash
# === Development Box 65 - 10.4.65.1  ===
10.4.65.5          payments.securetrading.net                   # DEV 65
10.4.65.2          stapi.securetrading.net                      # DEV 65
10.4.65.8          webservices.securetrading.net                # DEV 65
10.4.65.6          webapp.securetrading.net                     # DEV 65
10.4.65.3          myst.securetrading.net                       # DEV 65
10.4.65.10         externalclient.securetrading.net             # DEV 65
10.4.65.2          admin.securetrading.net                      # DEV 65
10.4.65.10         www.paypal.com                               # DEV 65
10.4.65.10         externalclient.thirdparty.com                # DEV 65
```
#### Integration with BrowserStack

1. To establish local testing please use this document https://www.browserstack.com/local-testing/automate
